
@extends('layout.master')
@section ('title')
Sign Up
@endsection
@section ('content')
<h1>Buat Account Baru</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method="POST">
    @csrf
    <label>First Name :</label><br>
    <input type="text" name="firstname"><br><br>
 <label>Last Name :</label><br>
 <input type="text" name="lastname"><br><br>
 <label>Gender</label><br>
 <input type="radio">Male<br>
 <input type="radio">Female<br><br>
 <label>Nationality</label><br>
 <select name="country">
     <option value="Indonesia">Indonesia</option>
     <option value="Malaysia">Malaysia</option>
     <option value="Singapura">Singapura</option>

 </select><br><br>

 <label>Language Spoken</label><br>
 <input type="checkbox" name="lspoken">Bahasa Indonesia<br>
 <input type="checkbox" name="lspoken">English<br>
 <input type="checkbox" name="lspoken">Other<br><br>
     

<label>Bio</label><br>
<textarea name="bio" rows="10" cols="30"></textarea>
<br>

<input type="submit" value="Sign Up">

     
</form>
<a href="/">Kembali ke Halaman Utama</a>
   
@endsection