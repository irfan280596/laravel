
@extends('layout.master')
@section ('title')

HALAMAN Edit List

@endsection
@section ('content')
<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" value="{{$cast->umur}}"class="form-control" >
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
  <label>Bio</label><br>
<textarea name="bio" rows="10" cols="30" class="form-control">{{$cast->bio}}</textarea>
</div>
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
<br>

<button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
