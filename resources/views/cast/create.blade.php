
@extends('layout.master')
@section ('title')

HALAMAN CAST

@endsection
@section ('content')
<form method="POST" action="/cast">
    @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control" >
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control" >
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
  <label>Bio</label><br>
<textarea name="bio" rows="10" cols="30" class="form-control"></textarea>
</div>
@error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
<br>

<button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection



