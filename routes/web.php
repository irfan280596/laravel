<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index' );
Route::get('/registrasi', 'AuthController@regis' );
Route::post('/welcome', 'AuthController@welcome' );


Route::get('/data_table', 'IndexController@datatable' );
//CRUD
//Create
Route::get('/cast/create', 'castController@create'); //form tambah data
Route::post('/cast', 'castController@store'); //menyimpan data form
//Read
Route::get('/cast','castController@index'); //ambil data
Route::get('/cast/{cast_id}','castController@show'); //route detail cast
//Update
Route::get('/cast/{cast_id}/edit','castController@edit'); //route form edit
Route::put('/cast/{cast_id}','castController@update'); //route form edit

//Delete
Route::delete('/cast/{cast_id}','castController@destroy'); //route delete



