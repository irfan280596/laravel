<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view ('page.register');
    }
    public function welcome (Request $request)
    {
        $namadepan = $request['firstname'];
        $namabelakang = $request['lastname'];
        $gender = $request['radio'];
        $nationality = $request['country'];
        $bahasa = $request['lspoken'];
        $bio = $request['bio'];

        return view('page.welcome',compact("namadepan","namabelakang"));
    }
}
